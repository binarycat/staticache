//! caching of function calls via static local variables.
//!
//! ideal for when you have an expensive function being repeatedly called with the same input.
//!
//! the use of static locals allows different call sites to have seperate caches.

#[derive(Default)]
pub struct Cache<I: Eq, O: Clone> {
	pair: Option<(I, O)>,
}

impl<I: Eq, O: Clone> Cache<I, O> {
	pub const fn new() -> Self {
		return Self{ pair: None };
	}
	fn get(&self, input: &I) -> Option<O> {
		if let Some((ref lastinput, ref output)) = self.pair {
			if input == lastinput {
				return Some(output.clone());
			}
		}
		return None;
	}
	pub fn get_or_call<F: Fn(&I)-> O>(&mut self, input: I, func: F) -> O {
		if let Some(output) = self.get(&input) {
			return output;
		} else {
			let output = func(&input);
			self.pair = Some((input, output.clone()));
			return output;
		}
	}
}



#[cfg(test)]
mod tests {
    use super::*;

	fn add(a: u32, b: u32) -> u32 {
		unsafe {
			static mut CACHE: Cache<(u32, u32), u32> = Cache::new();
			return CACHE.get_or_call((a, b), |(aa, bb)| aa + bb);
		}
	}

    #[test]
    fn it_works() {
        assert_eq!(add(1, 1), 2);
		assert_eq!(add(1, 1), 2);
		assert_eq!(add(1, 2), 3);
    }
}
